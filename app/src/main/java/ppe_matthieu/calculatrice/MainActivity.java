package ppe_matthieu.calculatrice;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    /* **********************************
     *           VARIABLE
     ********************************** */

    private double chiffre1;
    private boolean clicOperateur = false;
    private boolean update = false;
    private String operateur = "";
    TextView Result;
    Button btnUn;
    Button btnDeux;
    Button btnTrois;
    Button btnQuatre;
    Button btnCinq;
    Button btnSix;
    Button btnSept;
    Button btnHuit;
    Button btnNeuf;
    Button btnZero;
    //    Action
    Button btnClear;
    Button btnDelete;
    Button btnEqual;
    //    Opération
    Button btnPlus;
    Button btnMinus;
    Button btnMultiply;
    Button btnDivide;
    Button btnPoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* **********************************
         *           Initialisation
         ********************************** */
        //  Chiffres
        btnUn = findViewById(R.id.button1);
        btnDeux = findViewById(R.id.button2);
        btnTrois =  findViewById(R.id.button3);
        btnQuatre =  findViewById(R.id.button4);
        btnCinq =  findViewById(R.id.button5);
        btnSix =  findViewById(R.id.button6);
        btnSept =  findViewById(R.id.button7);
        btnHuit =  findViewById(R.id.button8);
        btnNeuf =  findViewById(R.id.button9);
        btnZero =  findViewById(R.id.button0);
        //    Action
        btnClear =  findViewById(R.id.buttonClean);
        btnDelete =  findViewById(R.id.buttonEffacer);
        btnEqual =  findViewById(R.id.buttonEqual);
        //    Opération
        btnPlus =  findViewById(R.id.buttonAdditionner);
        btnMinus =  findViewById(R.id.buttonSoustraire);
        btnMultiply =  findViewById(R.id.buttonMultiplier);
        btnDivide =  findViewById(R.id.buttonDiviser);
        btnPoint =  findViewById(R.id.buttonPoint);
        //    Affichage
        Result = findViewById(R.id.twResult);


        /* **********************************
                EVENEMENTS
         ***********************************/

        // Chiffres
        btnUn.setOnClickListener(v -> chiffreClick("1"));
        btnDeux.setOnClickListener(v -> chiffreClick("2"));
        btnTrois.setOnClickListener(v -> chiffreClick("3"));
        btnQuatre.setOnClickListener(v -> chiffreClick("4"));
        btnCinq.setOnClickListener(v -> chiffreClick("5"));
        btnSix.setOnClickListener(v -> chiffreClick("6"));
        btnSept.setOnClickListener(v -> chiffreClick("7"));
        btnHuit.setOnClickListener(v -> chiffreClick("8"));
        btnNeuf.setOnClickListener(v -> chiffreClick("9"));
        btnZero.setOnClickListener(v -> chiffreClick("0"));
        btnPoint.setOnClickListener(v -> chiffreClick("."));

        // Opérations
        btnPlus.setOnClickListener(v -> plusClick());
        btnMinus.setOnClickListener(v -> minusClick());
        btnMultiply.setOnClickListener(v -> multiplyClick());
        btnDivide.setOnClickListener(v -> divideClick());
        btnEqual.setOnClickListener(v -> equalClick());
        btnClear.setOnClickListener(v -> clearClick());
        btnDelete.setOnClickListener(v -> deleteClick());
    }


    /* **********************************
     *          FONCTIONS
     **********************************/
    //
    //voici la méthode qui est exécutée lorsqu'on clique sur un bouton chiffre
    public void chiffreClick(String str) {
        if (update) {
            update = false;
        } else {
            if (!Result.getText().equals("0"))
                str = Result.getText() + str;
        }
        Result.setText(str);
    }

    //voici la méthode qui est  exécutée lorsqu'on clique sur le bouton +
    public void plusClick() {

        if (clicOperateur) {
            calcul();
            Result.setText(String.valueOf(chiffre1));
        } else {
            chiffre1 = Double.valueOf(Result.getText().toString());
            clicOperateur = true;
        }
        operateur = "+";
        update = true;
    }

    //voici la méthode qui est  exécutée lorsqu'on clique sur le bouton -
    public void minusClick() {
        if (clicOperateur) {
            calcul();
            Result.setText(String.valueOf(chiffre1));
        } else {
            chiffre1 = Double.valueOf(Result.getText().toString());
            clicOperateur = true;
        }
        operateur = "-";
        update = true;
    }

    //voici la méthode qui est  exécutée lorsqu'on clique sur le bouton *
    public void multiplyClick() {
        if (clicOperateur) {
            calcul();
            Result.setText(String.valueOf(chiffre1));
        } else {
            chiffre1 = Double.valueOf(Result.getText().toString());
            clicOperateur = true;
        }
        operateur = "*";
        update = true;
    }

    //voici la méthode qui est  exécutée lorsqu'on clique sur le bouton /
    public void divideClick() {
        if (clicOperateur) {
            calcul();
            Result.setText(String.valueOf(chiffre1));
        } else {
            chiffre1 = Double.valueOf(Result.getText().toString());
            clicOperateur = true;
        }
        operateur = "/";
        update = true;
    }

    //voici la méthode qui est  exécutée lorsqu'on clique sur le bouton =
    public void equalClick() {
        calcul();
        update = true;
        clicOperateur = false;
    }

    //voici la méthode qui est  exécutée lorsque l'on clique sur le bouton C
    public void clearClick() {
        clicOperateur = false;
        update = true;
        chiffre1 = 0;
        operateur = "";
        Result.setText("");
    }

    //voici la méthode qui est  exécutée lorsque l'on clique sur le bouton effacer
    public void deleteClick() {
        clicOperateur = false;
        update = true;
        Result.setText(Result.getText().subSequence(0, Result.length() - 1));
    }

    //Voici la méthode qui fait le calcul qui a été demandé par l'utilisateur
    private void calcul() {
        if (operateur.equals("+")) {
            chiffre1 = chiffre1 + Double.valueOf(Result.getText().toString());
            Result.setText(String.valueOf(chiffre1));
        }

        if (operateur.equals("-")) {
            chiffre1 = chiffre1 - Double.valueOf(Result.getText().toString());
            Result.setText(String.valueOf(chiffre1));
        }

        if (operateur.equals("*")) {
            chiffre1 = chiffre1 * Double.valueOf(Result.getText().toString());
            Result.setText(String.valueOf(chiffre1));
        }

        if (operateur.equals("/")) {
            try {
                chiffre1 = chiffre1 / Double.valueOf(Result.getText().toString());
                Result.setText(String.valueOf(chiffre1));
            } catch (ArithmeticException e) {
                Result.setText("0");
            }
        }
    }


}
